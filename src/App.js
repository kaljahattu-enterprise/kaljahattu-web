import './App.css';
import { Layout, Menu  } from 'antd';
import { Typography } from 'antd';
import {
  SignalFilled,
  SettingFilled,
  SmileOutlined,
} from '@ant-design/icons';


import ConnectionComponent from './ConnectionComponent';
import SettingsComponent from './SettingsComponent';
import HealthComponent from './HealthComponent';
import React from 'react';
import { createStore, Store } from 'state-pool';

import BluetoothManager from './BluetoothManager';

const { Header } = Layout;

const bluetoothManager = new BluetoothManager();

window.bluetoothManager = bluetoothManager;

const settingsStore = createStore();

window.settingsStore = settingsStore;

settingsStore.subscribe(saveSettings);
loadStoredSettings();

function loadStoredSettings() {
  const storedSettings = localStorage.getItem('settings');
  if (storedSettings) {
    const settings = JSON.parse(storedSettings);
    window.settingsStore.setState("settings", settings);
  } else {
    window.settingsStore.setState("settings", {});
  }
};

function saveSettings(key, value) {
  if (key === "settings") {
    localStorage.setItem('settings', JSON.stringify(value));
  }
};

class App extends React.Component {
  defaultKey = '1';
  pages = {
    '1': {
      title: 'Connection',
      icon: <SignalFilled />,
      component: <ConnectionComponent />,
    },
    '2': {
      title: 'Health',
      icon: <SmileOutlined />,
      component: <HealthComponent />,
    },
    '3': {
      title: 'Settings',
      icon: <SettingFilled />,
      component: <SettingsComponent />,
    },
  };

  state = {
    current: this.pages[this.defaultKey],
  }

  handleMenuClick = e => {
    this.setState( {current: this.pages[e.key] });
  };

  render() {
    const { current } = this.state;
    const BodyComponent = current.component;

    return (
      <div className="App">
        <Layout>
          <Header>
            {/* <Typography.Title level={3} >{ current.title }</Typography.Title> */}
            <Menu theme="dark" mode="horizontal" onClick={this.handleMenuClick} defaultSelectedKeys={[this.defaultKey]}>
              { Object.entries(this.pages).map(([key, page]) => (
                <Menu.Item key={key} icon={ page.icon }>{/* page.title */}</Menu.Item>
              )) }
            </Menu>
          </Header>
          { BodyComponent }
        </Layout>
      </div>
    );
  }

}

export default App;
