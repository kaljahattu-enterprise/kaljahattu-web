import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import App from './App';

test('three menu items present', () => {
  render(<App />);
  const menuElements = screen.getAllByRole('menuitem')
  expect(menuElements).toHaveLength(3);
});