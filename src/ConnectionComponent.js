import React from "react";
import { Typography, Layout, Button, Space, Divider} from "antd";
import NerdStats from "./NerdStats";
import MotorControl from "./MotorControl";

const { Header, Content, Sider } = Layout;
const { Text, Title } = Typography;

class ConnectionComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      characteristics: window.bluetoothManager?.characteristics || [],
      settings: window.settingsStore?.getState("settings")?.value || null,
      connection: {
        connected: window.bluetoothManager?.server?.connected || false,
      },
    };
  }

  connectBLE = (e) => {
    window.bluetoothManager.connect()
    .then(() => {
      window.bluetoothManager.readAllCharacteristics();
      this.setState({
        connection: {
          connected: true,
        },
        characteristics: window.bluetoothManager.characteristics,
      });
    })
    .catch(error => {
      console.log(error);
    });
  };

  disconnectBLE = (e) => {
    window.bluetoothManager.disconnect()
    this.setState({
      connection: {
        connected: false,
      },
    });
  };

  render() {
    const connected = this.state.connection.connected;

    return (
      <Content className="connection-component-body">
        <Space direction="vertical">
          <Title>Connection</Title>
          <Text><Text strong>Connected: </Text>{ connected ? "yes" : "no" }</Text>
          <Button type="primary" disabled={ !this.state.settings?.drink } onClick={ connected ? this.disconnectBLE : this.connectBLE }>{ connected ? "Disconnect" : "Connect"}</Button>
          <Divider />
          { connected &&
          <Space direction="horizontal" align="start" size={16} wrap="true">
            <NerdStats />
            <MotorControl />
          </Space>
          }
        </Space>
      </Content>
    );
  }
}

export default ConnectionComponent;