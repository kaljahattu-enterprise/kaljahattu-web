class BluetoothManager {

    UUID = "19b20000-e8f2-537e-4f6c-d104768a1214";
    characteristic_UUIDs = [
        0x2a20, // Motor State
        0x2b20, // Motor Speed
        0x2c20, // Motor Direction

        0x2d20, // Drink Size
        0x2e20, // Drink Speed
        0x2f20, // Drink State
    ]

    device;
    server;
    service;
    characteristics;

    findCharacteristicByUUID(uuid) {
        if (this.characteristics) {
            return this.characteristics.find(characteristic => characteristic.uuid.includes(uuid));
        }
    }

    connect() {
        return navigator.bluetooth.requestDevice({ 
            filters: [{ services: ['19b20000-e8f2-537e-4f6c-d104768a1214']}],
            optionalServices: ['19b20000-e8f2-537e-4f6c-d104768a1214', '00001801-0000-1000-8000-00805f9b34fb']
        })
        .then(device => {
            this.device = device;
            return device.gatt.connect();
        })
        .then(server => {
            this.server = server;
            return server.getPrimaryService("19b20000-e8f2-537e-4f6c-d104768a1214");
        })
        .then(async service => {
            this.service = service;
            this.characteristics = await Promise.all(this.characteristic_UUIDs.map(uuid => service.getCharacteristic(uuid)));
        })
        .then(this.syncSettings.bind(this));
    }

    async syncSettings() {
        const settings = window.settingsStore.getState("settings").value;

        await this.findCharacteristicByUUID("2d20").writeValue(new Uint8Array([settings.drink.drink_size]))
        await this.findCharacteristicByUUID("2e20").writeValue(new Uint8Array([settings.drink.drink_speed]));

        return
    }

    disconnect() {
        if(this.server) {
            return this.server.disconnect();
        }
    }

    async readAllCharacteristics() {
        for (const characteristic of this.characteristics) {
            await characteristic.readValue();
        }
    } 
}

export default BluetoothManager;