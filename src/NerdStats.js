import React from "react";
import { Space, Typography, Table, Collapse } from "antd";

import { SyncOutlined } from "@ant-design/icons";

const { Text, Title } = Typography;

//React class that displays nerdy stats
export default class NerdStats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    columns = [
        {
            title: "UUID",
            dataIndex: "uuid",
            key: 'uuid',
        },
        {
            title: "Value",
            dataIndex: "value",
            key: 'value',
            render: (value) => {
                return (
                    <Text>{value ? value.getUint8(0) : ""}</Text>
                );
            },
            shouldCellUpdate: (record, prevRecord) => true,
        },
        {
            title: "Props",
            dataIndex: "properties",
            key: 'properties',
            render: (props) => {
                return (
                    <Space direction="vertical">
                        <Text>{props.read ? "Read" : ""}</Text>
                        <Text>{props.write ? "Write" : ""}</Text>
                        <Text>{props.notify ? "Notify" : ""}</Text>
                    </Space>
                );
            }
        },
    ];
    
    //Renders the component
    render() {
        return (
        <Space direction="vertical" className="nerd-stats">
            <Title level={2}>Stats for neerds!</Title>
            <Collapse defaultActiveKey={['1']}>
                <Collapse.Panel header="Characteristics" key="1">
                    <Table columns={this.columns} rowKey="uuid" dataSource={window.bluetoothManager?.characteristics} pagination={false} />
                </Collapse.Panel>
            </Collapse>
        </Space>
        );
    }
    }