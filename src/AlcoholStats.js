import React from "react";
import { Typography } from "antd";

export default class AlcoholStats extends React.Component {
  render() {
    return (
      <div>
        <Typography.Title level={3}>Current promille:</Typography.Title>
        <Typography.Text>{0.1*this.props.sips}</Typography.Text>
      </div>
    )
  }
}
