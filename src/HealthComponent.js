import React from "react";
import { Typography, Layout, Button, Space, Divider} from "antd";
import NerdStats from "./NerdStats";
import MotorControl from "./MotorControl";
import AlcoholStats from "./AlcoholStats"

const { Header, Content, Sider } = Layout;
const { Text, Title } = Typography;

class HealthComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      connection: {
        connected: true
      },
      sipsTaken: 0
    };
  }

  sipHappened() {
    this.setState({
      ...this.state,
      sipsTaken: this.state.sipsTaken + 1
    })
  }

  render() {
    let connected = this.state.connection.connected
    return (
      <Content className="connection-component-body">
        <Space direction="vertical">
          { connected &&
          <Space direction="horizontal" align="start" size={16} wrap="true">
            <MotorControl sipCallback={this.sipHappened.bind(this)}/>
            <AlcoholStats sips={this.state.sipsTaken}/>
          </Space>
          }
        </Space>
      </Content>
    );
  }
}

export default HealthComponent;
