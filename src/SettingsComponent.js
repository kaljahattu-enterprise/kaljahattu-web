import React from "react";
import { Typography, Layout, Button, Space, Divider, Card, Form, Input, InputNumber, Radio, message } from "antd";
import { formatCountdown } from "antd/lib/statistic/utils";

const { Header, Content, Sider } = Layout;
const { Text, Title } = Typography;

function SettingsComponent() {
  const formRef = React.createRef(null);

  const [healthSubmitDisabled, setHealthSubmitDisabled] = React.useState(true);
  const [drinkSubmitDisabled, setDrinkSubmitDisabled] = React.useState(true);

  const healthSelector = (settings) => settings.health;
  const healthPatcher = (settings, health) => { settings.health = health };

  const drinkSelector = (settings) => settings.drink;
  const drinkPatcher = (settings, drink) => { settings.drink = drink };

  const [healthSettings, setHealthSettings] = window.settingsStore.useState("settings", {selector: healthSelector, patcher: healthPatcher});
  const [drinkSettings, setDrinkSettings] = window.settingsStore.useState("settings", {selector: drinkSelector, patcher: drinkPatcher});

  const onHealthFinish = (values) => {
    setHealthSettings(values);
    setHealthSubmitDisabled(true);
    
    if(window.bluetoothManager?.server?.connected) {
      window.bluetoothManager.syncSettings();
    }
    
    message.success({
      content: "Health Settings Updated",
      duration: 1,
    });
  }

  const onDrinkFinish = (values) => {
    setDrinkSettings(values);
    setDrinkSubmitDisabled(true);

    if(window.bluetoothManager?.server?.connected) {
      window.bluetoothManager.syncSettings();
    }

    message.success({
      content: "Drink Settings Updated",
      duration: 1,
    });
  }

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <Content>
      <Title>Settings</Title>
      <Space direction="horizontal" wrap="true" align="start">
        <Card title="Health Information">
          <Form
            onValuesChange = {(changedValues, allValues) => {
              setHealthSubmitDisabled(false);
            }}
            name="Health Settings"
            labelCol={{ span: 16 }}
            wrapperCol={{ span: 16 }}
            initialValues={ healthSettings }
            autoComplete="off"
            onFinish={onHealthFinish}
            onFinishFailed={onFinishFailed}
            ref={formRef}
          >
            <Form.Item
              label="Age"
              name="age"
              rules={[{ required: true }]}
            >
              <InputNumber
              />
            </Form.Item>

            <Form.Item
              label="Weight (kg)"
              name="weight"
              rules={[{ required: true }]}
            >
              <InputNumber
              />
            </Form.Item>

            <Form.Item
              label="Sex"
              name="sex"
              rules={[{ required: true }]}
            >
              <Radio.Group>
                <Radio.Button value="male" >Male</Radio.Button>
                <Radio.Button value="female" >Female</Radio.Button>
              </Radio.Group>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit" disabled={ healthSubmitDisabled }>
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Card>
        <Card title="Drink preferences">
          <Form
            onValuesChange = {(changedValues, allValues) => {
              setDrinkSubmitDisabled(false);
            }}
            name="Settings"
            labelCol={{ span: 16 }}
            wrapperCol={{ span: 16 }}
            initialValues={ drinkSettings }
            autoComplete="off"
            onFinish={onDrinkFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="Hölä size (ml)"
              name="drink_size"
              rules={[{ required: true }]}
            >
              <InputNumber
              />
            </Form.Item>
            <Form.Item
              label="Drinking speed (ml)"
              name="drink_speed"
              rules={[{ required: true }]}
            >
              <Radio.Group>
                <Radio.Button value={100}>Slow</Radio.Button>
                <Radio.Button value={150}>Normal</Radio.Button>
                <Radio.Button value={200}>Fast</Radio.Button>
              </Radio.Group>
            </Form.Item>
            
            <Form.Item
              label="Pump offset (ml)"
              name="pump_offset"
              rules={[{ required: true }]}
            >
              <InputNumber
              />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit" disabled={ drinkSubmitDisabled }>
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </Space>
    </Content>
  );
}

export default SettingsComponent;