import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import ConnectionComponent from './ConnectionComponent';

// Expect title to exist
test('renders title', () => {
  render(<ConnectionComponent />);
  const titleElement = screen.getByText((content, element) => {
    return element.tagName.toLowerCase() === 'h1' && content === 'Connection'
  })
  expect(titleElement).toBeInTheDocument();
});

// Expect connected: to exist
test('Connected: status present', () => {
  render(<ConnectionComponent />);
  const statusElement = screen.getByText("Connected:");
  expect(statusElement).toBeInTheDocument();
});


// Expect button to read "Connect" 
test('Connect button reads "Connect"', () => {
  render(<ConnectionComponent />);
  const buttonElement = screen.getByRole("button", {name: "Connect"});
  expect(buttonElement).toBeInTheDocument();
});