import React from "react";
import { Card, Typography, Space, Collapse, Button, InputNumber, Checkbox } from "antd";

const { Text, Title } = Typography;

export default class MotorControl extends React.Component {
    constructor() {
        super();
        this.state = {
            motorEnabled: false,
            motorSpeed: 0,
        };
    }

    componentDidMount() {
        if(window?.bluetoothManager?.characteristics) {
            window.bluetoothManager.findCharacteristicByUUID('2b20').readValue().then( (value) => {
                this.setState({
                    motorSpeed: value.getUint8(0),
                });
            });
        }
    }

    toggleMotor = () => {
        this.setState({
            ...this.state,
            motorEnabled: !this.state.motorEnabled,
        }, () => {
            window.bluetoothManager.findCharacteristicByUUID("2a20").writeValue(new Uint8Array([this.state.motorEnabled ? 1 : 0]));
        });
    };

    setSpeed = (speed) => {
        this.setState({
            ...this.state,
            motorSpeed: speed,
        }, () => {
            window.bluetoothManager.findCharacteristicByUUID("2b20").writeValue(new Uint8Array([speed]));
        });
    };

    takeSip = () => {
        this.props.sipCallback()
        window.bluetoothManager.findCharacteristicByUUID("2f20").writeValue(new Uint8Array([1]));
    };

    render() {
        return (
            <Card>
                <Title level={2}>Motor Control</Title>
                <Space direction="horizontal" size="large">
                    <Button type="primary" danger={ this.state.motorEnabled } onClick={ this.toggleMotor }>{this.state.motorEnabled ? "Disable Motor" : "Enable Motor" }</Button>
                </Space>
                <div><Text strong>Speed: </Text><InputNumber min={0} max={255} value={ this.state.motorSpeed } onChange={ this.setSpeed } /></div>
                <Button type="primary" onClick={ this.takeSip }>Take a sip</Button>
            </Card>
        )
    }
}
